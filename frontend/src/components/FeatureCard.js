import { Flex, Text, Heading } from "@chakra-ui/react";

const FeatureCard = ({label,text,icon}) => {
    return(
        <Flex
            align='center'
            p='5'
            wrap='wrap'
            border='2px solid gray'
            _hover={{ shadow: '2xl' }}
        >
            <Flex
            direction='column'
            align='center'
            p='5'
            wrap='wrap'
            border='2px solid gray'
        >
            <Flex 
                align='center'
            >{icon}</Flex>
            <Heading
                as='h5'
                fontSize='15'
            >
                {label}
            </Heading>
            <Text
                fontSize='10'
                textAlign='center'
                mt='2'
            >
                {text}
            </Text>
        </Flex>

            
        </Flex>
    )
};

export default FeatureCard;