import { Flex } from '@chakra-ui/react';

const FormContainer = ({ children, width = 'xl' }) => {
	return (
		<Flex
			direction='column'
			boxShadow='lg'
			rounded='md'
			border='1px solid white'
			p='10'
			width={width}>
			{children}
		</Flex>
	);
};

export default FormContainer;
