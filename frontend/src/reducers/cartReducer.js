import { CART_ADD_ITEM, CART_REMOVE_ITEM, CART_SAVE_DELIVERY_ADDRESS, CART_SAVE_PAYMENT_METHOD} from '../constants/cartConstant';

export const cartReducer = (state = { cartItems: [] }, action) => {
	switch (action.type) {
		case CART_ADD_ITEM:
			const item = action.payload;
			const existItem = state.cartItems.find((i) => i.dish === item.dish);

			if (existItem) {
				return {
					...state,
					cartItems: state.cartItems.map((i) =>
						i.dish === existItem.dish ? item : i
					),
				};
			} else {
				return { ...state, cartItems: [...state.cartItems, item] };
			}
		
		case CART_REMOVE_ITEM:
			return {
				...state,
				cartItems: state.cartItems.filter((i) => i.dish !== action.payload),
			};

			case CART_SAVE_DELIVERY_ADDRESS:
				return { ...state, deliveryAddress: action.payload };
			case CART_SAVE_PAYMENT_METHOD:
				return { ...state, paymentMethod: action.payload };
		default:
			return state;
	}
};
