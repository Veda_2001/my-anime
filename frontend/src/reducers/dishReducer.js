import {
    DISH_CATEGORY_REQUEST,
    DISH_CATEGORY_SUCCESS,
    DISH_CATEGORY_FAIL,
    DISH_DETAILS_REQUEST,
    DISH_DETAILS_SUCCESS,
    DISH_DETAILS_FAIL,
} from '../constants/dishConstant';

export const dishCategoryReducer = (state = { filteredDishes: [] }, action) => {
    switch(action.type) {
        case DISH_CATEGORY_REQUEST:
            return { ...state, loading: true };
        case DISH_CATEGORY_SUCCESS: 
            return { loading: false, filteredDishes: action.payload };
        case DISH_CATEGORY_FAIL:
            return { loading: false, error: action.payload };
        default:
            return state;
    }
};

export const dishDetailsReducer = (state = { dish: { reviews: [] } }, action) => {
    switch(action.type) {
        case DISH_DETAILS_REQUEST:
            return { ...state, loading: true };
        case DISH_DETAILS_SUCCESS: 
            return { loading: false, dish: action.payload };
        case DISH_DETAILS_FAIL:
            return { loading: false, error: action.payload };
        default:
            return state;
    }
};

