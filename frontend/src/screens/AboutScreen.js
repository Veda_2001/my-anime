import { Flex, Heading, Image, Box, Text, Icon } from "@chakra-ui/react";
import {AiFillInstagram, AiFillTwitterCircle, AiOutlineWhatsApp, AiFillFacebook} from 'react-icons/ai';



const AboutScreen = () => {
    return (
        <Flex 
            direction='column'
            p='10'
            align='center'
            gap='10'
        >
            <Flex 
            direction={{base:'column', md:'row'}}
            p='10'
            align='center'
            gap='20'
            >
                <Image src='/images/about1.png' alt='about' objectFit='cover' />
                
                <Flex 
                    direction='column'
                    align='center'
                    gap='10'
                >
                    <Heading
                        as='h4'
                        fontFamily='serif'
                        fontWeight='bold'
                        align ='center'
                    >
                        We make <span style={{color:'red'}}>Food</span> with <span style={{color:'red'}}>Love</span> 
                        <br/> for <br/> 
                        <span style={{color:'red'}}>People</span> who <span style={{color:'red'}}>Love</span> to eat
                    </Heading>
                    <Text 
                        fontSize='sm'
                        align='center'
                    >
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </Text>
                </Flex>
            </Flex>

            <Flex 
            direction={{base:'column', md:'row'}}
            p='10'
            align='center'
            gap='20'
            >
                
                
                <Flex 
                    direction='column'
                    align='center'
                    gap='10'
                >
                    <Heading
                        as='h4'
                        fontFamily='serif'
                        fontWeight='bold'
                        align ='center'
                    >
                        <span style={{color:'red'}}>Dessert</span> is our speciality 
                    </Heading>
                    <Text 
                        fontSize='sm'
                        align='center'
                    >
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </Text>
                </Flex>
                <Image src='/images/mac.png' alt='about' objectFit='cover' h='80'/>
            </Flex>

                    {/* Contact */}

            <Flex
                as='footer'
                direction='column'
                bgColor='gray.200'
                alignItems='center'
                gap='5'
                p='10'
            >
                <Heading 
                    as='h5'
                    fontFamily='serif'
                    fontWeight='bold'
                    borderBottom='1px solid black'
                >
                    Contact Us
                </Heading>
                <Text 
                    fontSize='sm'
                >
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                        Excepteur cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        
                </Text>


                <Flex
                    direction='column'
                    p='5'
                >
                    <Text>
                        Email: abc@example.com
                    </Text>
                    <Text>
                        Phone: +91 1234567890
                    </Text>

                    <Box
                        mt='3'
                        align='center'
                    >
                        <Icon as={AiFillInstagram} mr='3' h='6' w='6' />
                        <Icon as={AiFillTwitterCircle} mr='3' h='6' w='6' />
                        <Icon as={AiOutlineWhatsApp} mr='3' h='6' w='6' />
                        <Icon as={AiFillFacebook} h='6' w='6' />
                    </Box>
                </Flex>
                
                
            </Flex>
        
        </Flex>
    )
};

export default AboutScreen;