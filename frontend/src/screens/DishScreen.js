import {Flex, Image, Text, Heading, Button, Icon, Box,Select} from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {useNavigate, Link as RouterLink, useParams} from 'react-router-dom';
import {detailsDish} from '../actions/dishAction';
import {BsArrowLeft,} from "react-icons/bs";
import Rating from '../components/Rating';
import Loader from '../components/Loader';
import Message from '../components/Message';


const DishScreen = () => {
   
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const {id} = useParams();

    const [qty, setQty] = useState(1);

    const dishDetails = useSelector((state) => state.dishDetails);
    const { loading, error, dish } = dishDetails;

    useEffect(() => {
        dispatch(detailsDish(id));
    },[id, dispatch]);
 
	const addToCartHandler = () => {
		navigate(`/cart/${id}?qty=${qty}`);
        
	};

    const incQty = () => {
        
    }  

    const checkoutHandler = () => {
		navigate(`/login?redirect=/delivery`);
	};

	

    return(
        <>
            <Flex
                borderBottom='1px solid white'
            >
				<Button as={RouterLink} to='/menu' colorScheme='facebook' mb='5'>
					<Icon as={BsArrowLeft}  w='5' h='5'/>
				</Button>
			</Flex>
            
            {loading ? (
				<Loader />
			) : error ? (
				<Message type='error'>{error}</Message>
			) : (
                <Box>
                    
                    <Flex
                        p='5'
                        alignItems='center'
                        justifyContent='center'
                        border='2px solid gray'  
                        bgColor='gray.200'
                    >
                        <Heading
                                as='h2'
                                fontSize='xl'
                                fontWeight='extrabold'  
                                                           
                        >
                                {dish.name}
                        </Heading> 
                    </Flex>

                    <Flex
                        p='5'
                        justifyContent='end'
                    >
                        <Rating color='yellow.500' value={dish.rating} ml='2'/>   
                    </Flex>

                    
                                       
                    <Flex
                        direction={{base:'column', md:'row'}}
                        py='5'
                        px='10'
                        gap='10'
                    >
                        <Image src={dish.image} alt={dish.name} h='450' objectFit='cover' />
                        <Flex
                            direction='column'
                            p='10'
                        >
                            <Heading
                                as='h3'
                                fontSize='lg'
                                mb='5'
                            >
                                {dish.name}
                            </Heading>  
                            <Text
                                fontSize='md'
                                mb='3'
                            >
                               <span style={{fontWeight:'bold', textDecoration:'underline'}}>Category</span> - {dish.category}
                            </Text>
                            <Text
                                fontWeight='bold'
                                textDecor='underline'
                            >
                                Description
                            </Text>
                            <Text
                                alignItems='center'
                                mb='8'
                            >
                              {dish.description}
                            </Text>

                            <Flex
                                gap='5'
                            >
                                <Button 
                                    colorScheme='facebook'
                                    _hover={{shadow:'lg',bgColor:'teal'}}
                                    onClick={checkoutHandler}
                                    >
                                    ₹ {dish.price}
                                </Button>

                                <Button 
                                    colorScheme='facebook'
                                    _hover={{shadow:'lg',bgColor:'teal'}}
                                    onClick={addToCartHandler}
                                    >
                                        Add to Cart
                                </Button>
                                <Flex justifyContent='space-between' py='2'>
								<Button
                                    onClick={incQty}
                                >
                                    +
                                </Button>
								
							</Flex>
                            </Flex>
                        </Flex>
                    </Flex>                         
                </Box>
            )}
        </>

    )
};

export default DishScreen;