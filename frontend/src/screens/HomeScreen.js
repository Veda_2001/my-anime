import { Flex, Image , Heading ,Link, Box, Text, Icon} from '@chakra-ui/react';
import { Link as RouterLink } from 'react-router-dom';
import WhyUsCard from '../components/WhyUsCard';
import HomeScreenRev from '../components/HomeScreenRev';
import {AiFillCheckCircle} from 'react-icons/ai';

const HomeScreen = () => {

    return (
        <>
                <Box>
                                {/* Intro */}
                    <Flex
                        direction={{base:'column', md:'row'}}
                        align='center'
                        gap='10'
                        p='20'
                        justifyContent='space-evenly'
                    >
                            {/* All the dishes and slogan */}
                        <Flex 
                            direction='column'
                            align='center'
                        >

                            {/* Column 1 */}
                            <Heading
                                as='h3'
                                fontFamily='serif'
                                fontWeight='bold'
                                alignItems='center'
                            >
                                What's your <span style={{color:'red'}}>mood</span>  to eat today ?
                            </Heading>
                            <Text
                                align='center'
                                fontSize='sm'
                            >
                                Etiam cursus condimentum vulputate. 
                                <br/>
                                Nulla nisi orci, vulputate at dolor et, 
                            </Text>
                            <Link
                                as={RouterLink}
                                to='/menu'
                                p='2'
                                mt='5'
                                bgColor='telegram.700'
                                borderRadius='10'
                                color= 'white'
                                _hover={{textDecor:'none'}}
                            >
                                See our Menu !!
                            </Link>
                            <Flex
                                p='3'
                                alignItems='center'
                            >
                                <Icon as={AiFillCheckCircle} mr='2' color='red' />
                                <Text>
                                    No Shipping Charges
                                </Text>

                                <Icon as={AiFillCheckCircle} ml='8' mr='2' color='red' />
                                <Text >
                                    Secure Checkout
                                </Text>
                            </Flex>
                        </Flex>

                        {/* Column 2 */}
                        <Image src='/images/home.png' alt='food from everywhere' h='500'  objectFit='cover'/>
                    
                    </Flex>




                    {/* What We Serve */}
                    <Flex  
                       direction='column'
                       align='center'
                       justifyContent='center'
                       p='20'
                    >
                        <Heading
                            as='h4'
                            fontFamily='serif'
                            fontWeight='bold'
                            alignItems='center'
                            mb='1'
                        >
                            What We <span style={{color:'red'}}>Serve</span>
                        </Heading>

                        <Text
                            align='center'
                            fontSize='sm'
                        >
                            Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, 
                        </Text>
                        <Flex
                            align='center'
                            p='10'
                        >
                            <Image src='/images/serve.png' alt='what_we_serve' objectFit='cover' />
                        </Flex>
                    </Flex>




                        {/* Why Us*/}
            
                    <WhyUsCard/>

                        {/* Reviews */}
                    <Flex
                      direction='column'
                      gap='3'
                      align='center'  
                    >
                        <Heading
                           as='h4'
                           fontFamily='serif'
                           fontWeight='bold'
                           alignItems='center' 
                        >
                            Our <span style={{color:'red'}}>Customers Reviews</span>
                        </Heading>
                        <Text
                            align='center'
                            fontSize='sm'
                            mb='2' 
                        >
                            Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, 
                        </Text>
                        
                        <Flex
                            direction={{base:'column', sm:'row'}}
                            px='20'
                            gap='10'
                        >
                            
					        <Flex
                            direction={{base:'column', sm:'row'}}
                            px='20'
                            gap='5'
                            >
                                <HomeScreenRev 
                                    name='John Doe'
                                    email='john@example.com'
                                    rating= {4.5}
                                    rev='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
                                />

                                <HomeScreenRev 
                                    name='John Doe'
                                    email='john@example.com'
                                    rating= {4.6}
                                    rev='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
                                />
                            </Flex>

                            <Image src='/images/rev.png'  alt='review' objectFit='cover' h='80' />
                        </Flex>

                        <Link
                            as={RouterLink}
                            to='/review'
                            p='2'
                            bgColor='telegram.700'
                            borderRadius='10'
                            color= 'white'
                            _hover={{textDecor:'none'}}
                        >
                            Write us a review
                        </Link>

                    </Flex>
                    
                </Box>

        </>
    )
};

export default HomeScreen;