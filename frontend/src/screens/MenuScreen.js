import { Grid, Flex, Button, Icon, Box } from '@chakra-ui/react';
import {useDispatch, useSelector} from 'react-redux';
import { useEffect, useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { categoryDish } from '../actions/dishAction';
import DishCard from "../components/DishCard";
import {BsArrowLeft} from "react-icons/bs";
import Loader from '../components/Loader';
import Message from '../components/Message';


const MenuScreen = () => {
    const dispatch = useDispatch();

    const dishCategory = useSelector((state) => state.dishCategory);
    const { loading, error, filteredDishes } = dishCategory;

    const [category,setCategory] = useState([]);

    useEffect(() => {
        
            if(category === 'Indian'){
                dispatch(categoryDish(category));
            }
            if(category === 'Korean'){
                dispatch(categoryDish(category));
            }
            if(category === 'Italian'){
                dispatch(categoryDish(category));
            }
            if(category === 'Japanese'){
                dispatch(categoryDish(category));
            }
            if(category === 'Indo-Chinese'){
                dispatch(categoryDish(category));
            }
            if(category === 'Chinese'){
                dispatch(categoryDish(category));
            }
            if(category === 'Sea Food'){
                dispatch(categoryDish(category));
            }
            if(category === 'Dessert'){
                dispatch(categoryDish(category));
            }
        },[category, dispatch]);
    
    
    return(
        <>
                <Flex
                    borderBottom='1px solid white'
                >
                    <Button as={RouterLink} to='/' colorScheme='twitter' mb='5'>
                        <Icon as={BsArrowLeft} color='white' w='5' h='5'/>
                    </Button>
                </Flex>
                
                
                    {/* Menu Bar */}

                {loading ? (
                <Loader />
                ) : error ? (
                    <Message type='error'>{error}</Message>
                ) : (
                    <Box>
                        <Flex

                        alignItems='center'
                        justifyContent='center'
                        gap='10'
                        p='5'
                        mb='5'
                        bgColor='gray.200'
                        border='1px solid gray'
                        >
                            
                            <Button
                                _hover={{shadow:'lg'}}
                                colorScheme='facebook'
                                onClick={() => setCategory('Indian')}
                            >
                                Indian
                            </Button>
                            <Button
                                _hover={{shadow:'lg'}}
                                colorScheme='facebook'
                                onClick={() => setCategory('Korean')}
                            >
                                Korean
                            </Button>
                            <Button
                                _hover={{shadow:'lg'}}
                                colorScheme='facebook'
                                onClick={() => setCategory('Japanese')}
                            >
                                Japanese
                            </Button>
                            <Button
                                _hover={{shadow:'lg'}}
                                colorScheme='facebook'
                                onClick={() => setCategory('Chinese')}
                            >
                                Chinese
                            </Button>
                            <Button
                                _hover={{shadow:'lg'}}
                                colorScheme='facebook'
                                onClick={() => setCategory('Indo-Chinese')}
                            >
                                Indo-Chinese
                            </Button>
                            <Button
                                _hover={{shadow:'lg'}}
                                colorScheme='facebook'
                                onClick={() => setCategory('Sea Food')}
                            >
                                Sea-Food
                            </Button>
                            <Button
                                _hover={{shadow:'lg'}}
                                colorScheme='facebook'
                                onClick={() => setCategory('Dessert')}
                            >
                                Dessert
                            </Button>


                        </Flex>
                        <Grid
                                gridTemplateColumns='1fr 1fr 1fr'
                                gap='10'>
                                {filteredDishes.map((item) => (
                                    <DishCard key={item._id} dish={item} />
                                ))}
                        </Grid> 
                    </Box>              
                )}
        </>
    )
};

export default MenuScreen;