import {
	Button,
	Flex,
	FormControl,
    Image,
	FormLabel,
	Heading,
	HStack,
	Radio,
	RadioGroup,
	Spacer,
} from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { savePaymentMethod } from '../actions/cartAction';
import CheckoutSteps from '../components/CheckoutSteps';
import FormContainer from '../components/FormContainer';

const PaymentScreen = () => {
	const dispatch = useDispatch();
	const navigate = useNavigate();

	const cart = useSelector((state) => state.cart);
	const { deliveryAddress, paymentMethod } = cart;

	const [paymentMethodRadio, setPaymentMethodRadio] = useState(
		paymentMethod || 'paypal'
	);

	useEffect(() => {
		if (!deliveryAddress) {
			navigate('/delivery');
		}
	}, [navigate, deliveryAddress]);

	const submitHandler = (e) => {
		e.preventDefault();
		dispatch(savePaymentMethod(paymentMethodRadio));
		navigate('/forder');
	};

	return (
		<Flex w='full' alignItems='center' justifyContent='center' py='5'>
            <Image src='/images/payement.png' alt='pay' objectFit='cover'/>
			<FormContainer>
				<CheckoutSteps step1 step2 step3 />

				<Heading as='h2' mb='8' fontSize='3xl'>
					Payment Method
				</Heading>

				<form onSubmit={submitHandler}>
					<FormControl as='fieldset'>
						<FormLabel as='legend'>Select Methods</FormLabel>
						<RadioGroup
							value={paymentMethodRadio}
							onChange={setPaymentMethodRadio}>
							<HStack space='24px'>
								<Radio value='paypal'>PayPal or Credit/Debit Card</Radio>
							</HStack>
						</RadioGroup>
					</FormControl>

					<Spacer h='3' />

					<Button type='submit' colorScheme='facebook' mt='4'>
						Continue
					</Button>
				</form>
			</FormContainer>
		</Flex>
	);
};

export default PaymentScreen;
